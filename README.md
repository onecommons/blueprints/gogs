## Application Blueprint for Gogs

### What is Gogs?
Gogs aims to build a simple, stable and extensible self-hosted Git service with a simple and easy setup. Written in Go, Gogs supports all platforms that Go does, including Linux, macOS, Windows and ARM.

[Here is the link to application website home page](https://gogs.io/docs).

[This application blueprint uses the `gogs/gogs` Docker image](https://hub.docker.com/r/gogs/gogs).

### Filling out a deployment blueprint
For the general steps to deploy an application with Unfurl Cloud, see our [Getting Started guides](https://unfurl.cloud/help#guides).

#### Details
To simplify the deployment process, we have set default values for several required environment variables and hidden them from the UI. The remaining environment variables are exposed as the following “Detail” fields, which you must fill in after selecting a deployment:

| Input Name       | Default Value |
|------------------|---------------|
| Run Crond        | false         |
| Backup Retention | 7d            |

Below is a list of the hidden default environment variables and their values. As explained above, you will not encounter these environment variables as you fill in the deployment blueprint:

| Input Name      | Default Value                   |
|-----------------|---------------------------------|
| Back Arg Config | /app/gogs/example/custom/config |

#### Components
Each application blueprint includes **components**. These are the required and optional resources for the application. In most cases, there is more than one way to fulfill a component requirement. After you select a deployment blueprint, you will be prompted to fulfill the component requirements and configure the deployment to your needs. Common components include Compute, DNS, Database, and Mail Server:

1. **Compute** _Gogs requires the following compute resources._
   - At least 1 CPU(s).
   - At least 1024 MB of RAM.
   - At least 10 GB of hard disk space.

2. **DNS** _You must specify a DNS provider and zone. Supported DNS providers include:_
   - Google
   - DigitalOcean
   - AWS
   - Azure

3. **SQL Database**

    You can fulfill this requirement with one of the following:

    | Database Name | Compatible Versions |
    |---------------|---------------------|
    | MySQL         | Version >= 5.7      |
    | PostgreSQL    | Veersion 13 , 14    |

#### Required vs “Extras”
Some components are required for a deployment to succeed. Others, found under the “Extras” tab, are optional resources that enhance the features or performance of your deployment.
